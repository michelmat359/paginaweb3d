var renderer, scene, camera;


function init(){
    renderer = new THREE.WebGLRenderer();
    renderer.setSize (window.innerWidth, window.innerHeight);
    renderer.setClearColor (new THREE.Color(0xFFFFFF, 1.0));
    document.body.appendChild(renderer.domElement);

    scene = new THREE.Scene();

    var aspectRatio = window.innerWidth / window.innerHeight;
    camera = new THREE.PerspectiveCamera(75, aspectRatio, 0.1, 100);
    camera.position.set( 0, 1, 8 );
}
function loadScene(){
    //var geometria = new THREE.TetrahedronGeometry();
    var geometriaCubo = new THREE.CubeGeometry(2,2,2);
    var geometriaEsfera = new THREE.SphereGeometry(1);
    var material = new THREE.MeshBasicMaterial({color:0xFF0000, wireframe: true});
    //var tetraedro = new THREE.Mesh(geometria,material);
    var cubo = new THREE.Mesh(geometriaCubo, material);
    var esfera = new THREE.Mesh(geometriaEsfera, material);
    var esferaCubo = new THREE.Object3D();

    var geometriaTexto = new THREE.TextGeometry('Three.js',
        {
            size : 1, height: 0.1, curveSegments: 3,
            font: "helvetiker", weight: "bold", styuke:"normal",
            bevelThickness: 0.05, bevelSize: 0.04, bevelEnabled: true   
        }
    );
    var texto = new THREE.Mesh( geometriaTexto, material);
    texto.position.set(-3, 0, 2);
    texto.castShadow = true;
    scene.add(texto);

    //esferaCubo.add(esfera);
    //esferaCubo.add(cubo);
    //scene.add(tetraedro);
    //scene.add(esferaCubo);
}

function loadCubo(lado){
    var malla = new THREE.Geometry();
    var semilado = lado/2.0;
    var coordenadas = [    
        semilado,-semilado, semilado,  // 0
        semilado,-semilado,-semilado,  // 1
        semilado, semilado,-semilado,  // 2
        semilado, semilado, semilado,  // 3
       -semilado, semilado, semilado,  // 4
       -semilado, semilado,-semilado,  // 5
       -semilado,-semilado,-semilado,  // 6
       -semilado,-semilado, semilado   // 7 
       ];
       var colores =     [ 
        0xFF0000,   // 0
        0xFF00FF,   // 1
        0xFFFFFF,   // 2
        0xFFFF00,   // 3
        0x00FF00,   // 4
        0x00FFFF,   // 5
        0x0000FF,   // 6
        0x000000    // 7
         ];

    var indices = [
       0,3,7, 7,3,4, 0,1,2,
       0,2,3, 4,3,2, 4,2,5,
       6,7,4, 6,4,5, 1,5,2,
       1,6,5, 7,6,1, 7,1,0,
    ];

  // Construye vertices y los inserta en la malla
  for(var i=0; i<coordenadas.length; i+=3) {
    var vertice = new THREE.Vector3( coordenadas[i], coordenadas[i+1], coordenadas[i+2] );
    malla.vertices.push( vertice );
  }

    for (var i=0; i<indices.length; i+=3){
        var triangulo = new THREE.Face3( indices[i], indices[i+1], indices[i+2]);
        for (var j= 0; j<3; j++){
            var color = new THREE.Color( colores[ indices[i+j]]);
            triangulo.vertexColors.push(color);
        }
        malla.faces.push (triangulo);
    }
    var material = new THREE.MeshBasicMaterial( { vertexColors: THREE.VertexColors});
    var cubo = new THREE.Mesh( malla, material);

    scene.add(cubo);

}

function loadMesh(){
    var material = new THREE.MeshBasicMaterial( { color: "red" , wireframe: true});
    var ms = new THREE.Matrix4();
    var mt = new THREE.Matrix4();
    var tela = new THREE.Mesh( new THREE.CylinderGeometry ( 0.0, 1.0, 1.0), material);
    tela.matrixAutoUpdate = false;
    mt.makeTranslation(0, 1.5, 0);
    ms.makeScale(2, 0.5, 2);
 

    tela.matrix = mt.multiply(ms);

    var baston = new THREE.Mesh (new THREE.CylinderGeometry(1, 1, 1), material);
    baston.position.y = 0.5;
    baston.scale.set(0.05, 3, 0.05);
    var mango = new THREE.Mesh (new THREE.BoxGeometry(1, 1, 1), material);
    mango.scale.set(0.2 ,0.4, 0.2);
    mango.position.set(0, -1, 0);

    paraguas = new THREE.Object3D();
    paraguas.add(tela);
    paraguas.add(baston);
    paraguas.add(mango);
    paraguas.position.set(1.6, 0, 0);
    paraguas.rotation.x = Math.PI/6;

    scene.add(paraguas);
}

function update(){}

function render(){
    requestAnimationFrame(render);
    update();
    renderer.render(scene, camera);
}


init();
loadScene();
//loadCubo(3.0);
//loadMesh();
render();